package com.example.myapplication

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.VolleyError
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.fragment_city.*
import org.json.JSONObject
import kotlin.math.ceil

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "lat"
private const val ARG_PARAM2 = "long"

/**
 * A simple [Fragment] subclass.
 * Use the [CityFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class CityFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)



        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        getJsonData(param1,param2)
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_city, container, false)

    }


    private fun getJsonData(latitude: String?, longitude: String?) {
       // val API_KEY = "fae7190d7e6433ec3a45285ffcf55c86"
        // Instantiate the RequestQueue.
        val queue = Volley.newRequestQueue(activity?.applicationContext)
        val url = "http://api.openweathermap.org/data/2.5/weather?lat=${latitude}&lon=${longitude}&appid=fae7190d7e6433ec3a45285ffcf55c86"

        val jsonRequest = JsonObjectRequest(
            Request.Method.GET, url, null,
            { response ->
                setValues(response)
            },
            object : Response.ErrorListener {
                override fun onErrorResponse(volleyError: VolleyError) {
                    Toast.makeText(activity, "error,"+volleyError.toString()+"", Toast.LENGTH_LONG).show()
                    Log.d("erorrrrrrrrrrrrrrrr", "Error: " + volleyError
                            + "\nStatus Code " + volleyError.networkResponse.statusCode
                            + "\nCause " + volleyError.cause
                            + "\nnetworkResponse " + volleyError.networkResponse.data.toString()
                            + "\nmessage" + volleyError.message);

                }
            })

// Add the request to the RequestQueue.
        queue.add(jsonRequest)
    }

    private fun setValues(response: JSONObject?) {
        if (response != null) {
            city.text = response.getString("name")
            var lat = response.getJSONObject("coord").getString("lat")
            var long = response.getJSONObject("coord").getString("lon")
            coordinates.text = "${lat} , ${long}"
            weather.text = response.getJSONArray("weather").getJSONObject(0).getString("main")
            var tempr = response.getJSONObject("main").getString("temp")
            tempr = ((((tempr).toFloat() - 273.15)).toInt()).toString()
            temperature.text = "${tempr}°C"
            var mintemp = response.getJSONObject("main").getString("temp_min")
            mintemp = ((((mintemp).toFloat() - 273.15)).toInt()).toString()
            min_temp.text = "${mintemp}°C"

            var maxtemp = response.getJSONObject("main").getString("temp_max")
            maxtemp = ((ceil((maxtemp).toFloat() - 273.15)).toInt()).toString()
            max_temp.text = "${maxtemp}"

            pressure_val.text = response.getJSONObject("main").getString("pressure")+"hPa"

            humidity_val.text = response.getJSONObject("main").getString("humidity")+"%"

            windspeed_val.text = response.getJSONObject("wind").getString("speed")+"km/h"

            gust_val.text = response.getJSONObject("wind").getString("gust")
        }
    }

}