package com.example.myapplication

import android.graphics.Color
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions

/**
 * An activity that displays a Google map with a marker (pin) to indicate a particular location.
 */
// [START maps_marker_on_map_ready]
class MapsMarkerActivity : AppCompatActivity(), OnMapReadyCallback {



    // [START_EXCLUDE]
    // [START maps_marker_get_map_async]
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // Retrieve the content view that renders the map.
        setContentView(R.layout.activity_maps)
        window.statusBarColor = Color.parseColor("#0A0A0A")
      /*  if (getString(R.string.maps_api_key).isEmpty()) {
            Toast.makeText(this, "Add your own API key in MapWithMarker/app/secure.properties as MAPS_API_KEY=YOUR_API_KEY", Toast.LENGTH_LONG).show()
        }*/
        
        // Get the SupportMapFragment and request notification when the map is ready to be used.
        val mapFragment = supportFragmentManager.findFragmentById(R.id.map) as? SupportMapFragment
        mapFragment?.getMapAsync(this)


    }
    // [END maps_marker_get_map_async]
    // [END_EXCLUDE]

    // [START maps_marker_on_map_ready_add_marker]
    override fun onMapReady(googleMap: GoogleMap?) {


        googleMap?.setOnMapClickListener {
                latlng -> // Clears the previously touched position
            googleMap.clear();
            // Animating to the touched position
            googleMap.animateCamera(CameraUpdateFactory.newLatLng(latlng));

            val location = LatLng(latlng.latitude, latlng.longitude)
            googleMap.addMarker(MarkerOptions().position(location))

            val bundle = Bundle()
            bundle.putString("lat", latlng.latitude.toString())
            bundle.putString("long", latlng.longitude.toString())

            val cityFragment= CityFragment()
            cityFragment.arguments = bundle


            val transaction = supportFragmentManager.beginTransaction()
            transaction.replace(R.id.map, cityFragment)
            transaction.disallowAddToBackStack()
            transaction.commit()

        }


        /*  googleMap?.apply {
              val sydney = LatLng(-33.852, 151.211)
              addMarker(
                  MarkerOptions()
                      .position(sydney)
                      .title("Marker in Sydney")
                      .draggable(true)
              )
              // [START_EXCLUDE silent]
              moveCamera(CameraUpdateFactory.newLatLng(sydney))
              // [END_EXCLUDE]
          }*/
    }



    // [END maps_marker_on_map_ready_add_marker]
}